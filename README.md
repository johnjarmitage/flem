## fLEM

fLEM is a landscape evolution model written in Python using the FEniCS library to solve for sediment transport as described in [Smith & Bretherton, Water Resources Research (1972)](https://agupubs.onlinelibrary.wiley.com/doi/abs/10.1029/WR008i006p01506). The code is published in [Armitage, Earth Surface Dynamics (2019)](https://www.earth-surf-dynam.net/7/67/2019/esurf-7-67-2019.html). In biref, the rate of change in elevation of landscpe can be described as the balance between uplift and the flux of mass due to sediment transport. BY assuming that sediment transport is a function of surface run-off then a diffusive set of equations can be derived, and these can be solved numerically. I solve the system of equations using finite elements.

Flow routing is an important aspect of the model. I found that the best option is to route water down slope from node to node, where water collects along the edge of each element. I have implemented a simple distributive flow routing and steepest slope of descent. Output can be visualised using Paraview or equivalent.

# Requirements

- This code requires fenics and dolfin (tested with version 1.6.0 using python2).
- This code also uses [peakutils](https://zenodo.org/badge/latestdoi/10288304).

# Running the model

The code is run from the script run_models.py, which then calls the function transport_func and flow_func. There is another script that runs the code on an input DEM, transport_dem.py.