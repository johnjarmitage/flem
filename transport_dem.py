#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
FEniCS program: Smith & Bretherton (1972) equations

  u'= nabla((1+De*q_w).nabla(u)) + f  in the domain
  u = 0                               on the boundaries
  u = 0                               at t = 0

  u = elevation
  f = uplift
  
@author: armitage
"""

from __future__ import print_function
from fenics import *
from mshr import *
import numpy as np
from flow_func import *
from load_initial_func import *
import matplotlib.pyplot as plt
import peakutils # https://zenodo.org/badge/latestdoi/102883046

# Output filename
outname = 'dems/Ebro/modelX/ebro'

# Domain resolution
res = 1028

# Load dem
filename = 'dems/Ebro/srtm_paleo_30m.mat'
mesh,V,u_n,lx,ly = load_initial_func(filename,res)

#plot(mesh,interactive=False)
#plot(u_n,interactive=False)

# Physical parameters
kappa = 1e-1           # diffusion coefficient
c     = 1e-5           # discharge transport coefficient
nexp  = 1.5            # discharge exponent
alpha = 0.1            # precipitation rate
De    = c*pow(alpha*ly,nexp)/kappa
uamp  = 1e-4*ly/kappa  # uplift

num_steps = 200                # number of time steps
dt        = 1e1*kappa/(ly*ly)  # time step size

sed_flux = np.zeros(num_steps) # array to store sediment flux
time     = np.zeros(num_steps)

class East(SubDomain):
    def inside(self, x , on_boundary):
       return near(x[0], lx/ly)

class West(SubDomain):
    def inside(self, x , on_boundary):
       return near(x[0], 0.0)

class North(SubDomain):
    def inside(self, x , on_boundary):
       return near(x[1], ly/ly)

class South(SubDomain):
    def inside(self, x , on_boundary):
       return near(x[1], 0.0)

bc = DirichletBC(V, u_n, North())

#bc = [DirichletBC(V, u_n, North()),
#      DirichletBC(V, u_n, East())]

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
f = Constant(uamp)

q_n = flow_distrib(mesh,V,u_n,De,nexp)

F = u*v*dx + dt*q_n*dot(grad(u), grad(v))*dx - (u_n + dt*f)*v*dx
a, L = lhs(F), rhs(F)

# Solution and sediment flux
u = Function(V)
q_s = Expression('u0 + displ - u1',u0=u_n,displ=Constant(uamp*dt),u1=u,degree=2)

# Iterate
t = 0
i = 0
for n in range(num_steps):

    # Update current time
    t += dt
        
    # Compute solution
    solve(a == L, u, bc)

    # Plot solution
#    plot(u)
#    plot(q_n)
    
    # Calculate sediment flux
    sed_flux[i] = assemble(q_s*dx(mesh))
    time[i]     = t
    i += 1

    # Update previous solution
    u_n.assign(u)

    # Update flux
    q = flow_distrib(mesh,V,u_n,De,nexp)
    q_n.assign(q)
    
    if (n == 0 | n == num_steps-3):
      # Output elevation
      filename = '%s_u_solution_%d_%04d.pvd' % (outname,res,n)
      vtkfile = File(filename)
      vtkfile << u
      
      # Output water flux
      filename = '%s_q_solution_%d_%04d.pvd' % (outname,res,n)
      vtkfile = File(filename)
      
      vtkfile << q
    
# Hold plot
#interactive()
    
# Output elevation
filename = '%s_u_solution_%d_%04d.pvd' % (outname,res,n)
vtkfile = File(filename)
vtkfile << u

# Output water flux
filename = '%s_q_solution_%d_%04d.pvd' % (outname,res,n)
vtkfile = File(filename)

vtkfile << q

# Post processing
plt.plot(time*1e-6*ly*ly/kappa,sed_flux/dt*kappa,'k',linewidth=2)
plt.xlabel('Time (Myr)')
plt.ylabel('Sediment Flux (m^2/yr)')
plt.savefig('dems/Ebro/modelX/ebro_sed_flux.svg',format='svg')






