# -*- coding: utf-8 -*-
"""
Plot wavelength from channel to channel

Created on Mon Feb  5 13:24:14 2018

@author: armitage
"""

import numpy as np
import matplotlib.pyplot as plt

wave  = np.genfromtxt('../wavelengths_celldistrib01.txt')
ress  = wave[:,0]
width = wave[:,2]
plt.figure()
plt.plot(ress,width,'+k')

wav64  = np.zeros(200)
wav128 = np.zeros(200)
wav256 = np.zeros(200)
wav512 = np.zeros(200)
wav1024 = np.zeros(200)

i = 0
j = 0
k = 0
l = 0
m = 0
o = 0
for res in ress:
  if res == 64:
    wav64[i] = width[m]
    i += 1
  if res == 128:
    wav128[j] = width[m]
    j += 1
  if res == 256:
    wav256[k] = width[m]
    k += 1
  if res == 512:
    wav512[l] = width[m]
    l += 1
  if res == 1024:
    wav1024[o] = width[m]
    o += 1
  m += 1

x  = [64,128,256,512]
y  = np.zeros(len(x))
y_ = np.zeros(len(x))

y[0] = np.mean(wav64)
y[1] = np.mean(wav128)
y[2] = np.mean(wav256)
y[3] = np.mean(wav512)
#y[1] = np.mean(wav1024)
y_[0] = np.std(wav64)
y_[1] = np.std(wav128)
y_[2] = np.std(wav256)
y_[3] = np.std(wav512)
#y_[1] = np.std(wav1024)

plt.figure()
plt.errorbar(x,y,yerr=y_)
ys = [wav64,wav128,wav256,wav512]
plt.figure()
plt.boxplot(ys)
plt.ylim((0,0.35))
plt.xlabel('Resolution')
plt.ylabel('Normalised wavelength')
plotname = 'wavelengths_celldistrib01.svg'
plt.savefig(plotname,format='svg')
plt.show()