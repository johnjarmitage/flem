# -*- coding: utf-8 -*-
"""
Created on Mon May 28 10:34:16 2018

@author: armitage
"""

import numpy as np
import matplotlib.pyplot as plt

kappa = 1e+0
ly    = 8e5
U     = 1e-4
dt    = 1e4*kappa/(ly*ly)

resolutions = [64,128,256,512]
number      = np.linspace(0,9,10)
colors      = ['r','g','b','k']

sed  = np.genfromtxt('../sedfulxes_celldistrib01.txt')
NUMB = sed[:,0]
RESO  = sed[:,1]
TIME  = sed[:,2]
QS    = sed[:,3]

j = 0
for res in resolutions :
  times = TIME[np.where(RESO == res)]
  seds  = QS[np.where(RESO == res)]
  nums  = NUMB[np.where(RESO == res)]
  for num in number :
    plt.plot(times[np.where(nums == num)],seds[np.where(nums == num)],colors[j])
  j += 1

Tr = 3*1e-4*ly/1e-4*1e-6
plt.plot((Tr,Tr),(0,25),'k--')  
plt.xlim((0,5))
plt.ylim((0,25))
plt.xlabel('Time (Myr)')
plt.ylabel('Sediment Flux (m^2/yr)')




plotname = 'sed_flux_celldistrib01.svg'
plt.savefig(plotname,format='svg')