# -*- coding: utf-8 -*-
"""
Created on Wed Jan 24 16:35:03 2018

Script to run transport_func() many times!

@author: armitage
"""

from __future__ import print_function
from fenics import *
from mshr import *
import numpy as np
from transport_func import *
import matplotlib.pyplot as plt
import peakutils # https://zenodo.org/badge/latestdoi/102883046
import os

# Directory name
name = 'celldistrib01'
wavename = 'wavelengths_%s.txt' % name
sedname = 'sedfulxes_%s.txt' % name
dirtest = './%s/' % name
directory = os.path.dirname(dirtest)
if not os.path.exists(directory) :
  os.makedirs(directory)

# Plotting parameters (check that they are the same as in transport_func)
ly    = 8e5
kappa = 1e+0
dt    = 1e4*kappa/(ly*ly)

# Model parameters (input)
num_steps   = 500                 # number of time steps
resolutions = [64, 128, 256, 512]
number      = np.linspace(0,9,10)

QS   = np.zeros((len(resolutions),len(number),num_steps))
TIME = np.zeros((len(resolutions),len(number),num_steps))
WAVE = np.zeros((len(resolutions),len(number),20))

f    = open(wavename,'w')
fsed = open(sedname,'w')

i = 0
for res in resolutions :
  j = 0
  for num in number :
    [QS[i,j,:],TIME[i,j,:],WAVE[i,j,:]] = transport_func(res,num,num_steps,name)
    k = 0
    for iw in range(20) :
      f.write('%d\t%d\t%g\n' % (res,num,WAVE[i,j,k]))
      k += 1
    j += 1
  i += 1

#i = 0
#for res in resolutions :
#  k = 0
#  for iw in range(20) :
#    plt.plot(resolutions[i]*np.ones(len(number)),WAVE[i,:,k],'+k')
#    k += 1
#  i += 1
#plt.xlabel('Resolution')
#plt.ylabel('Wavelength')
#plotname = '%s/wavelengths.svg' % name
#plt.savefig(plotname,format='svg')
#plt.clf()

j = 0
for num in number :
  plt.plot(TIME[0,j,:]*1e-6*ly*ly/kappa,QS[0,j,:]/dt*kappa,'r')
  plt.plot(TIME[1,j,:]*1e-6*ly*ly/kappa,QS[1,j,:]/dt*kappa,'g')
  plt.plot(TIME[2,j,:]*1e-6*ly*ly/kappa,QS[2,j,:]/dt*kappa,'b')
  plt.plot(TIME[3,j,:]*1e-6*ly*ly/kappa,QS[3,j,:]/dt*kappa,'k')
  
  k = 0
  for res in resolutions:
    for t in range(num_steps):
      fsed.write('%d\t%d\t%g\t%g\n' % (num,res,TIME[k,j,t]*1e-6*ly*ly/kappa,QS[k,j,t]/dt*kappa))
    k += 1
  j += 1
plt.xlabel('Time (Myr)')
plt.ylabel('Sediment Flux (m^2/yr)')
plotname = '%s/sed_flux.svg' % name
plt.savefig(plotname,format='svg')
plt.clf()

f.close()
fsed.close

