#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
FEniCS program: Smith & Bretherton (1972) equations

  u'= nabla((1+De*q_w).nabla(u)) + f  in the domain
  u = 0                               on the boundaries
  u = 0                               at t = 0

  u = elevation
  f = uplift
  
@author: armitage
"""

from __future__ import print_function
from fenics import *
from mshr import *
import numpy as np
from flow_func import *
import matplotlib.pyplot as plt
import peakutils # https://zenodo.org/badge/latestdoi/102883046

def transport_func(res,num,num_steps,name):

    # Domain dimensions
    lx = 2e5
    ly = 8e5
       
    # Physical parameters
    kappa = 1e+0           # diffusion coefficient
    c     = 1e-4           # discharge transport coefficient
    nexp  = 1.5            # discharge exponent
    alpha = 1              # precipitation rate
    De    = c*pow(alpha*ly,nexp)/kappa
    uamp  = 1e-4*ly/kappa     # uplift
    
    dt        = 1e4*kappa/(ly*ly)   # time step size
    
    sed_flux = np.zeros(num_steps) # array to store sediment flux
    time     = np.zeros(num_steps)
    
    # Create mesh and define function space
    domain = Rectangle(Point(0,0), Point(lx/ly,ly/ly))
    mesh = generate_mesh(domain,res);
        
    V = FunctionSpace(mesh,'P',1)
    
    # Define boundary condition and intial condition
    # u_D = Expression('100/lx*x[0]',degree=1,lx=lx)
    u_D = Constant(0)
    
    class East(SubDomain):
        def inside(self, x , on_boundary):
           return near(x[0], lx/ly)
    
    class West(SubDomain):
        def inside(self, x , on_boundary):
           return near(x[0], 0.0)
    
    class North(SubDomain):
        def inside(self, x , on_boundary):
           return near(x[1], ly/ly)
    
    class South(SubDomain):
        def inside(self, x , on_boundary):
           return near(x[1], 0.0)
    
    bc = [DirichletBC(V, u_D, West()),
          DirichletBC(V, u_D, East())]
    
    # def boundary(x, on_boundary):
    #   return on_boundary
    # bc = DirichletBC(V, u_D, boundary)
    
    
    
    # Define initial value
    eps = 10/ly;
    u_n = interpolate(u_D, V)
    u_n.vector().set_local(u_n.vector().array()+eps*np.random.random(u_n.vector().size()))
    
    # Define variational problem
    u = TrialFunction(V)
    v = TestFunction(V)
    f = Constant(uamp)
    
    q_n = flow_celldistrib(mesh,V,u_n,De,nexp)
    
    F = u*v*dx + dt*q_n*dot(grad(u), grad(v))*dx - (u_n + dt*f)*v*dx
    a, L = lhs(F), rhs(F)
    
    # Solution and sediment flux
    u = Function(V)
    q_s = Expression('u0 + displ - u1',u0=u_n,displ=Constant(uamp*dt),u1=u,degree=2)
    
    # Iterate
    t = 0
    i = 0
    for n in range(num_steps):
    
        # Double rain fall
        if n == 1199:
          alpha = 2
          De    = c*pow(alpha*ly,nexp)/kappa
    
        # Update current time
        t += dt
            
        # Compute solution
        solve(a == L, u, bc)
    
        # Calculate sediment flux
        sed_flux[i] = assemble(q_s*dx(mesh))
        time[i]     = t
        i += 1
    
        # Update previous solution
        u_n.assign(u)
    
        # Update flux
        q = flow_celldistrib(mesh,V,u_n,De,nexp)
        q_n.assign(q)
        
        # Output last few solutions
        if n >= num_steps-50 :
          filename = '%s/u_solution_%d_%d_%d.pvd' % (name,res,num,n)
          vtkfile = File(filename)
          vtkfile << u
          filename = '%s/q_solution_%d_%d_%d.pvd' % (name,res,num,n)
          vtkfile = File(filename)
          vtkfile << q
      
    # Post processing
    plt.plot(time*1e-6*ly*ly/kappa,sed_flux/dt*kappa,'k',linewidth=2)
    plt.xlabel('Time (Myr)')
    plt.ylabel('Sediment Flux (m^2/yr)')
    sedname = '%s/sed_flux_%d_%d.svg' % (name,res,num)
    plt.savefig(sedname,format='svg')
    plt.clf()
    
    # Output last elevation
    filename = '%s/u_solution_%d_%d_%d.pvd' % (name,res,num,n)
    vtkfile = File(filename)
    vtkfile << u
    
    # Output last water flux
    filename = '%s/q_solution_%d_%d_%d.pvd' % (name,res,num,n)
    vtkfile = File(filename)
    vtkfile << q
    
    # Calculate valley spacing from peak to peak in water flux
    tol    = 0.001 # avoid hitting points outside the domain
    y      = np.linspace(0 + tol, 1 - tol, 100)
    x      = np.linspace(0.01,0.21,20)
    wavelength = np.zeros(len(x))
    i = 0
    for ix in x :
      points = [(ix, y_) for y_ in y] # 2D points
      q_line = np.array([q(point) for point in points])
          
      indexes = peakutils.indexes(q_line,thres=0.05,min_dist=5)
      if len(indexes) > 1 :
        wavelength[i] = sum(np.diff(y[indexes]))/(len(indexes)-1)
      else :
        wavelength[i] = 0
      
      plt.plot(y*1e-3*ly,q_line*kappa/ly,'k',linewidth=2)
      plt.plot(y[indexes]*1e-3*ly,q_line[indexes]*kappa/ly,'+r')
      i += 1
    
    plt.xlabel('Distance (km)')
    plt.ylabel('Water Flux (m/yr)')
    watername = '%s/water_flux_spacing_%d_%d.svg' % (name,res,num)
    plt.savefig(watername,format='svg')
    plt.clf()
    
    return(sed_flux,time,wavelength)
    
